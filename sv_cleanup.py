# TODO FEATURE print out missing begin/end blocks: after if, else, assert
# TODO FEATURE max chars per line: remove all newlines and add them back? how to treat comments?
# TODO KNOWN ISSUES:
#  - if and else do not cause indentation if begin/end is missing
#  - assertions do not cause indentation (after label or after condition?)

import sys
import os
import re
import io

class SVCleaner:

   def __init__(self):

      #------------------------------------------------------------------------------------------------------------------------------------
      # Options
      #------------------------------------------------------------------------------------------------------------------------------------

      self.OPTIONS = {
         # clean files in subfolders or only in current folder
         'CLEAN_FILES_IN_SUBFOLDERS': False,

         # perform indentation
         'FIX_INDENTATION': True,
         'INDENTATION_STEP': None, # use None to auto detect indentation
         'MAX_ONE_INDENT_PER_LINE': True, # for when a line contains multiple keywords that start a block

         # remove trailing spaces
         'FIX_TRAILING_SPACES': True,

         # replace tabs with spaces
         'FIX_TABS': True,
         'SPACES_PER_TAB': 4,

         # enforce at least a space between code and comment //
         'SPACE_BEFORE_COMMENT': True,

         # enforce newline after keyword
         'NEWLINE_AFTER_KEYWORD': {
            'begin': True,
            'end': True,
            '^fork': None, # forks may have labels
            'join': True,
            'join_any': True,
            'join_none': True,
         },

         # enforce newline before keyword
         'NEWLINE_BEFORE_KEYWORD': {
            'begin': None, # begin might be at end of line (for loops) or at the start of line (fork processes)
            'end': True,
            'endcase': True,
            'endgroup': True,
            'endclass': True,
            'endfunction': True,
            'endtask': True,
            'for': True,
            '^fork': True,
            'join': True,
            'join_any': True,
            'join_none': True,
         },

         # enforce maximum one statement per line
         'ONE_STATEMENT_PER_LINE' : True,

         # ignore block comments: /* ... */
         'IGNORE_BLOCK_COMMENTS' : True,
      }

      #------------------------------------------------------------------------------------------------------------------------------------
      # Defines
      #------------------------------------------------------------------------------------------------------------------------------------

      self.keywords = ['begin', 'end', 'if', 'else', 'for', 'foreach', 'forever', 'while', 'case', 'endcase',
                       'fork', 'join', 'join_any', 'join_none', 'property', 'endproperty', 'sequence', 'endsequence',
                       'function', 'endfunction', 'task', 'endtask', 'class', 'endclass', 'covergroup', 'endgroup']

      # block starts after the following words are found in a line ('^' means start of line, after indentation)
      self.block_start = ['begin', 'case', 'covergroup', '^fork', '^class', # avoid "wait fork;", "typedef class" forward declaration
                          '^function', '^task', '^virtual function', '^virtual task', # avoid "extern task" and "extern function"
                          'uvm_object_utils_begin', 'uvm_object_param_utils_begin',
                          'uvm_component_utils_begin', 'uvm_component_param_utils_begin']
      # block ends when the following words are found in a line
      self.block_end = ['end', 'endcase', 'endgroup', 'join', 'join_none', 'join_any', 'endclass', 'endfunction', 'endtask',
                        'uvm_object_utils_end', 'uvm_object_param_utils_end',
                        'uvm_component_utils_end', 'uvm_component_param_utils_end']

   #------------------------------------------------------------------------------------------------------------------------------------
   # Function that checks for some text in a line, except inside strings ""
   #------------------------------------------------------------------------------------------------------------------------------------
   def search_regex_except_in_strings(self, x, line):
      line_no_strings = ''
      lst = line.split("\"")
      for item_idx, item in enumerate(lst):
         if item_idx % 2 == 0:
            line_no_strings += lst[item_idx]

      # search for whole words only
      return re.search(r'\b' + x + r'\b', line_no_strings)

   #------------------------------------------------------------------------------------------------------------------------------------
   # Function that returns the indentation to be used, based on options
   #------------------------------------------------------------------------------------------------------------------------------------
   def get_indentation_step(self, lines):
      # auto detect indentation step
      if self.OPTIONS['INDENTATION_STEP'] is None:
         supp_inds = {1:0, 2:0, 3:0, 4:0, 5:0}
         for line_ind in lines['indentation']:
            cur_line_ind = re.sub(r'\t', ' ' * self.OPTIONS['SPACES_PER_TAB'], line_ind).count(' ')
            if min(x for x in supp_inds) < cur_line_ind <= max(x for x in supp_inds):
               supp_inds[cur_line_ind] += 1

         print('Found indentation = %s' % max(supp_inds, key=supp_inds.get))
         return max(supp_inds, key=supp_inds.get)

      # use defined indentation step
      else:
         return self.OPTIONS['INDENTATION_STEP']

   #------------------------------------------------------------------------------------------------------------------------------------
   # Function that opens the and populates the 3 lists (with one element per line): indentation, code and comments
   #------------------------------------------------------------------------------------------------------------------------------------
   def get_lines_from_file(self, filename):
      lines = {}
      lines['indentation'] = []
      lines['code'] = []
      lines['comment'] = []

      inside_block_comment = False

      # parse the input file and get the lines
      with open(filename, 'r') as in_f:
         full_lines = in_f.readlines()

         for line in full_lines:
            # if inside a block comment, treat all lines as a big comment on one line
            if inside_block_comment is True:
               if '*/' in line:
                  inside_block_comment = False
               lines['comment'][-1] += line
            else:
               # split until 1st match with the '//' separater and any whitespace preceeding it (round brackets save the separator in resulting list!)
               split_line = re.split(r'([ \t]*\/\/)', line, 1)

               # add to the comments list the line comment separator '//", all its preceding whitespace and everything after it (including newline)
               if len(split_line) == 1:
                  # if the line did not contain comments, add the newline removed from the code list to the comments list
                  lines['comment'].append('\n')
               elif len(split_line) == 3:
                  lines['comment'].append(split_line[1] + split_line[2])
               else:
                  raise Exception('split_line length = %s, split_line = split_line' % (len(split_line), split_line))

               # split further to get the indentation and the code separately (newline is always part of comments list)
               split_line = re.split(r'(^[ |\t]*)', split_line[0].rstrip('\n'))

               # in python 2, the split becomes a list of 1 element if no code is present on the line
               if len(split_line) == 1:
                  split_line = [split_line[0], '', '']

               # the first item returned to the list is empty string and can be ignored
               lines['indentation'].append(split_line[1])
               lines['code'].append(split_line[2])

               # check for start of block comment inside code (it cannot start in a line comment)
               block_comment_start = lines['code'][-1].split('/*', 1)
               if len(block_comment_start) > 1 and self.OPTIONS['IGNORE_BLOCK_COMMENTS'] is True:
                  # check if end of block comment is not found later in the same line
                  if '*/' not in block_comment_start[1]:
                     # overwrite code and comment
                     lines['code'][-1] = block_comment_start[0]
                     lines['comment'][-1] = '/*' + block_comment_start[1] + lines['comment'][-1]
                     inside_block_comment = True

      return lines

   #------------------------------------------------------------------------------------------------------------------------------------
   # Function that performs the clean up steps
   #------------------------------------------------------------------------------------------------------------------------------------
   def cleanup(self, input_file):
      print('Started cleaning up file %s...' % input_file)

      # check for windows or linux end of line
      with open(input_file, 'rb') as in_f:
         eol = '\r\n' if '\\r\\n' in str(in_f.readlines()) else '\n'

      # get the indentation, code and comment from the lines of the input file
      lines = self.get_lines_from_file(input_file)

      ind_step = self.get_indentation_step(lines)

      #------------------------------------------------------------------------------------------------------------------------------------
      # 1. replace tabs with spaces
      #------------------------------------------------------------------------------------------------------------------------------------
      if self.OPTIONS['FIX_TABS'] is True:
         for item in lines:
            for line_idx, line in enumerate(lines[item]):
               lines[item][line_idx] = re.sub(r'\t', ' ' * self.OPTIONS['SPACES_PER_TAB'], line)

      #------------------------------------------------------------------------------------------------------------------------------------
      # 2. enforce max one statement per line
      #------------------------------------------------------------------------------------------------------------------------------------
      if self.OPTIONS['ONE_STATEMENT_PER_LINE'] is True:
         for line_idx, line in enumerate(lines['code']):
            # split the line by ';' to get the number of statements
            split_line = lines['code'][line_idx].split(';')

            lines['code'][line_idx] = ''

            # add back the ';' after split
            for i in range(len(split_line)):
               lines['code'][line_idx] += split_line[i]

               if i < len(split_line) - 1:
                  lines['code'][line_idx] += ';'

               if i < len(split_line) - 2:
                  # add newline after all statements that have code with ';' after it that doesn't contain a keyword
                  if all([re.search(r'\b' + x + r'\b', lines['code'][line_idx]) is None for x in self.keywords]):
                     lines['code'][line_idx] += '\n'

      #------------------------------------------------------------------------------------------------------------------------------------
      # 3. enforce at least a space between code and comment
      #------------------------------------------------------------------------------------------------------------------------------------
      if self.OPTIONS['SPACE_BEFORE_COMMENT'] is True:
         for line_idx, line in enumerate(lines['code']):
            if lines['code'][line_idx] != '' and lines['comment'][line_idx].startswith('//'):
               lines['comment'][line_idx] = ' ' + lines['comment'][line_idx]

      #------------------------------------------------------------------------------------------------------------------------------------
      # 4. handle newlines before/after keywords (adding lines takes priority over removing lines)
      #------------------------------------------------------------------------------------------------------------------------------------
      # remove newlines before keyword
      for keyword in self.OPTIONS['NEWLINE_BEFORE_KEYWORD']:
         if self.OPTIONS['NEWLINE_BEFORE_KEYWORD'][keyword] == False:
            for line_idx, line in enumerate(lines['code']):
               if re.search(r'^' + keyword + r'\b', lines['code'][line_idx]) is not None:
                  lines['code'][line_idx] = lines['code'][line_idx-1] + ' ' + lines['code'][line_idx]
                  lines['comment'][line_idx] = lines['comment'][line_idx-1].strip('\n') + ' ' + lines['comment'][line_idx].lstrip().strip('\n') + '\n'

                  # remove the previous line, which was merged with this one
                  lines['indentation'].pop(line_idx-1)
                  lines['code'].pop(line_idx-1)
                  lines['comment'].pop(line_idx-1)

      # remove newlines after keyword
      for keyword in self.OPTIONS['NEWLINE_AFTER_KEYWORD']:
         if self.OPTIONS['NEWLINE_AFTER_KEYWORD'][keyword] == False:
            for line_idx, line in enumerate(lines['code']):
               if re.search(r'\b' + keyword + '\s*$', lines['code'][line_idx]) is not None:
                  # merge together this line and the following line
                  lines['code'][line_idx] = lines['code'][line_idx] + ' ' + lines['code'][line_idx+1]
                  lines['comment'][line_idx] = lines['comment'][line_idx].strip('\n') + ' ' + lines['comment'][line_idx+1].lstrip().strip('\n') + '\n'

                  # remove from the list of lines the following line
                  lines['indentation'].pop(line_idx+1)
                  lines['code'].pop(line_idx+1)
                  lines['comment'].pop(line_idx+1)

      # add newlines before keyword
      for keyword in self.OPTIONS['NEWLINE_BEFORE_KEYWORD']:
         if self.OPTIONS['NEWLINE_BEFORE_KEYWORD'][keyword] == True:
            for line_idx, line in enumerate(lines['code']):
               # first split code line by \", in order to ignore comments
               lst = lines['code'][line_idx].split('\"')

               new_code_line = ''
               for k,v in enumerate(lst):
                  # add the \" back, except for the last item in the list
                  if k + 1 != len(lst):
                     v += '"'

                  # only add newlines to code not in comments
                  if k % 2 == 0:
                     v = re.sub(r'(.*?[^ |\t])[ |\t]' + r'\b' + keyword + r'\b', r'\1' + '\n' + lines['indentation'][line_idx] + keyword.replace('^', ''), v)

                  new_code_line += v

               lines['code'][line_idx] = new_code_line

      # add newline after keyword
      for keyword in self.OPTIONS['NEWLINE_AFTER_KEYWORD']:
         if self.OPTIONS['NEWLINE_AFTER_KEYWORD'][keyword] == True:
            for line_idx, line in enumerate(lines['code']):
               # indentation for new line is the same as the old one
               new_line_indentation = lines['indentation'][line_idx]
               # exception if the keyword is a block starting keyword, the code moved to new line should be further indented
               if keyword in self.block_start:
                  new_line_indentation += ind_step * ' '

               # replace all keywords followed by whitespace with keyword + newline + indentation
               lines['code'][line_idx] = re.sub(r'\b' + keyword + r' |\t', keyword.replace('^', '') + '\n' + new_line_indentation, line)

      #------------------------------------------------------------------------------------------------------------------------------------
      # after newlines are enforced, reload the lines lists by writing and reading a temporary file
      #------------------------------------------------------------------------------------------------------------------------------------
      temp_file = input_file.rsplit('.', 1)[0] + '_temp.' + input_file.rsplit('.', 1)[1]
      with open(temp_file, 'w') as tmp_f:
         result = ''
         for i, v in enumerate(lines['code']):
            result += lines['indentation'][i] + lines['code'][i] + lines['comment'][i]
         tmp_f.write(result)

      # load the new lines
      lines = self.get_lines_from_file(temp_file)
      os.remove(temp_file)

      assert len(lines['indentation']) == len(lines['code']) == len(lines['comment']), \
         'Unequal number of lines for indentation (%s), code (%s) and comments (%s)!' % (lines['indentation'], lines['code'], lines['comment'])

      #------------------------------------------------------------------------------------------------------------------------------------
      # 5. fix indentation and trailing spaces
      #------------------------------------------------------------------------------------------------------------------------------------

      # recreate the full lines
      lines_full = []
      for idx, v in enumerate(lines['code']):
         lines_full.append(lines['indentation'][idx] + lines['code'][idx] + lines['comment'][idx])

      if self.OPTIONS['FIX_INDENTATION'] is True:
         # first, remove current indentation by doing left strip
         lines_full = [x.lstrip() for x in lines_full]
         # left strip will remove end of line from empty lines... add it back
         lines_full = ['\n' if x == '' else x for x in lines_full]

         block_indent = 0
         brackets_open = 0
         statement_indent = False

         for line_idx, line in enumerate(lines['code']):
            #------------------------------------------------------------------------------------------------------------------------------------
            # perform indentation for blocks of code
            #------------------------------------------------------------------------------------------------------------------------------------
            for keyword in self.block_end:
               if self.search_regex_except_in_strings(keyword, line) is not None:
                  block_indent -= 1

            # add block indentation for this line (except if it starts with block comment)
            if not lines['comment'][line_idx].startswith('/*'):
               lines_full[line_idx] = block_indent * ind_step * ' ' + lines_full[line_idx]

            assert block_indent >= 0, 'negative block indent = %s, at line index = %s' % (block_indent, line_idx)

            for keyword in self.block_start:
               if self.search_regex_except_in_strings(keyword, line) is not None:
                  block_indent += 1

            #------------------------------------------------------------------------------------------------------------------------------------
            # perform indentation for brackets: round, square and curly
            #------------------------------------------------------------------------------------------------------------------------------------
            # save the bracket count from previous line and calculate bracket count for this line
            brackets_open_prev_line = brackets_open
            brackets_open += line.count('(') + line.count('{') + line.count('[') - line.count(')') - line.count('}') - line.count(']')

            if brackets_open_prev_line > 0:
               # do not indent if this is the line that closes all brackets and contains no code
               if brackets_open < brackets_open_prev_line and re.search(r'[^ |\t|\)|\]|\}]', lines['code'][line_idx]) is None:
                  lines_full[line_idx] = ind_step * brackets_open * ' ' + lines_full[line_idx]
               else:
                  #print(line_idx+1, brackets_open, brackets_open_prev_line)
                  lines_full[line_idx] = ind_step * brackets_open_prev_line * ' ' + lines_full[line_idx]

            #------------------------------------------------------------------------------------------------------------------------------------
            # perform indentation for statements on multiple lines
            #------------------------------------------------------------------------------------------------------------------------------------

            # statement indentation is done only on the lines following the first line of the unfinished statement
            if statement_indent == True:
               lines_full[line_idx] = ind_step * ' ' + lines_full[line_idx]

            # check that the line doesn't contain any keywords
            no_keywods = all([re.search(r'\b' + keyword + r'\b', lines['code'][line_idx]) is None for keyword in self.keywords])
            # check that the line doesn't contain any of the following: ';' for end of statement, ':' for case statements, '`' for macros
            no_end_case_macros = re.search(r'[;|:|`]', lines['code'][line_idx]) is None
            # check that no brackets are opened and not closed on previous lines
            no_brackets_open = brackets_open_prev_line == 0 and brackets_open == 0
            # check that the line contains at least a letter, to avoid empty lines or lines with comments only
            has_letters = re.search(r'[a-zA-Z]', lines['code'][line_idx]) is not None

            # if all the above conditions are true, the current line is considered an unfinished statement, so the following line(s) will be indented, until ';' is found
            if no_keywods is True and no_end_case_macros is True and no_brackets_open is True and has_letters is True:
               statement_indent = True
            else:
               statement_indent = False

         # go again through all lines to enforce max one indentation per line
         cur_indentation = 0
         remove_indentation = 0
         until = 0
         if self.OPTIONS['MAX_ONE_INDENT_PER_LINE'] is True:
            for line_idx, line in enumerate(lines_full):
               # count indentation of current line and compare it with previous line
               prev_indentation = cur_indentation
               cur_indentation = re.split(r'[^ |\t]', lines_full[line_idx], 1)[0].count(' ') / ind_step

               if cur_indentation - prev_indentation > 1:
                  remove_indentation = cur_indentation - prev_indentation - 1
                  until = prev_indentation + 1

               if cur_indentation <= until:
                  remove_indentation = 0
                  until = 0
               else:
                  if remove_indentation > 0:
                     lines_full[line_idx] = lines_full[line_idx][ind_step:]
                     #print('Removing %s indentations from line %s: %s' % (remove_indentation, line_idx, lines_full[line_idx]))
               # if the indentation went to the right by more than one (ex: 3->5), save prev indentation (3) and make current line 4 instead of 5
               # keep removing one indentation from the following lines until indentation is back at 3

      if self.OPTIONS['FIX_TRAILING_SPACES'] is True:
         lines_full = [x.rstrip() + '\n' for x in lines_full]

      #------------------------------------------------------------------------------------------------------------------------------------
      # write output file with the same name and "_clean" suffix
      #------------------------------------------------------------------------------------------------------------------------------------

      # append _clean to output file
      input_file_list = input_file.rsplit('.', 1)
      output_file = input_file_list[0] + '_clean.' + input_file_list[1]

      # write the output file (with original end of line)
      with io.open(output_file, 'w', newline=eol) as out_f:
         result = ''
         for line in lines_full:
            result += line

         out_f.write(result)

 #------------------------------------------------------------------------------------------------------------------------------------
 # main program
 #------------------------------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
   list_of_files = []
   cleaner = SVCleaner()

   # only one file, given as argument
   if len(sys.argv) == 2:
      list_of_files = [sys.argv[1]]

   else:
      # all files in current folder and subfolders
      if cleaner.OPTIONS['CLEAN_FILES_IN_SUBFOLDERS'] is True:
         # get all .sv files in the subfolders
         for path, subdirs, files in os.walk('.'):
            for name in files:
               if name.rsplit('.', 1)[-1] == 'sv' and '_clean.sv' not in name:
                  list_of_files.append(path + '/' + name)

      # only files in current folder
      else:
         list_of_files = [x for x in os.listdir() if os.path.isfile(x) and x.endswith('.sv') and '_clean.sv' not in x]

   # clean up the file(s)
   for input_file in list_of_files:
      cleaner.cleanup(input_file)

   print('Job done!')
